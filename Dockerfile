FROM python:3.7

COPY my_package /app/my_package
COPY tests /app/tests

COPY requirements.txt /app
COPY tox.ini /app
COPY setup.cfg /app
COPY setup.py /app
WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT python ./my_package/main.py