from setuptools import setup, find_packages

setup(
    name='Efficient Dev',
    version='0.0.0',
    py_modules=['my_package'],
    # FIXME: pull all files with wsgi file
    packages=find_packages(),
    package_dir={'my_package': '.'}
)